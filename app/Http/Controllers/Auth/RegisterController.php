<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'       => ['required', 'string', 'max:255'],
            'email'      => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'   => ['required', 'string', 'min:6', 'confirmed'],
            'tos_read'   => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        if ($data['newsletter'] == 1 || $data['newsletter'] == '1') {
            $newsletter = true;
        } else {
            $newsletter = false;
        }

        if ($data['tos_read'] == 1 || $data['tos_read'] == '1') {
            $tos_read = true;
        } else {
            $tos_read = false;
        }

//        dd($data);
//        return User::create([
//            'name'       => $data['name'],
//            'email'      => $data['email'],
//            'tos_read'   => $tos_read,
//            'newsletter' => $newsletter,
//            'password'   => Hash::make($data['password']),
//        ]);
        return User::create([
            'name'       => $data['name'],
            'email'      => $data['email'],
            'tos_read'   => $data['tos_read'],
            'newsletter' => $data['newsletter'],
            'password'   => Hash::make($data['password']),
        ]);
    }
}
