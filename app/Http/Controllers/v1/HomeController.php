<?php

namespace App\Http\Controllers\v1;

use App\Models\Pages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function tos()
    {
        return view('tos');
    }
    public function privacy()
    {
        return view('privacy');
    }


    private function httpCurl($url){
        if(gethostbyname($url) != $url )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function pages($id = false)
    {

        if ($id) {
            $data = Pages::find($id);
//            dd($data->url);
            $code =  $this->httpCurl($data->url);
//           dd($code);
//            dd($data->id);
            return view('home.page')->with('page', $data)->with('code', $code);
        } else {
            $data = Pages::paginate(9);
            return view('home.pages')->with('pages', $data);
        }

    }

    public function search(Request $req)
    {
//        dd($req);
        $data = Pages::where('id', $req->input('search'))
            ->orWhere('title', 'like', '%' . $req->input('search') . '%')
            ->orWhere('description', 'like', '%' . $req->input('search') . '%')
            ->paginate(9);

//        dd($data);

        return view('home.pages')->with('pages', $data);
    }


    public function create()
    {
//        You can check if http:// is at the beginning of the string using strpos().
//        $var = 'www.somesite.com';
//        if(strpos($var, 'https://') !== 0) {
//            return 'https://' . $var;
//        } else {
//            return $var;
//        }
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
