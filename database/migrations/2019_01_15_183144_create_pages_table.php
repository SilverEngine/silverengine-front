<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('image');
            $table->string('title');
            $table->string('title_seo');
            $table->text('description');
            $table->string('url');
            $table->boolean('secure')->default(0);
            $table->string('company');
            $table->integer('views')->default(0);
            $table->double('loadtime')->default(0);
            $table->boolean('sitemap')->default(0);
            $table->boolean('api')->default(0);
            $table->string('platform');
            $table->integer('points')->default(0);
            $table->boolean('level1')->default(0);
            $table->boolean('level2')->default(0);
            $table->boolean('level3')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
