<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if (env('APP_ENV') == 'local') {
            DB::table('users')->insert([
                'name'              => 'neo',
                'group_id'          => 2,
                'email'             => 'nejc@badget.com',
                'email_verified_at' => '2019-01-04 02:00:00.000000',
                'password'          => bcrypt('nejc1991'),
                'credit'            => 50000,
                'tos_read'          => '1',
                'newsletter'        => '1',
            ]);
        }

        DB::table('pages')->insert([
            'user_id'     => 1,
            'image'       => 'http://localhost/silverengine/public/site/01.png',
            'title'       => 'SilverEngine',
            'title_seo'   => 'SilverEngine',
            'description' => 'soon',
            'secure'      => 1,
            'url'         => 'https://silverengine.net',
            'company'     => 'MIT project',

            'platform' => 'laravel',
        ]);
    }
}
