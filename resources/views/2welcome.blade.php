@extends('layouts.master')

@section('css')
    <style>
        .timeline {
            position: relative;
            padding: 0;
            list-style: none
        }

        .timeline:before {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 40px;
            width: 2px;
            margin-left: -1.5px;
            content: '';
            background-color: #e9ecef
        }

        .timeline > li {
            position: relative;
            min-height: 50px;
            margin-bottom: 50px
        }

        .timeline > li:after, .timeline > li:before {
            display: table;
            content: ' '
        }

        .timeline > li:after {
            clear: both
        }

        .timeline > li .timeline-panel {
            position: relative;
            float: right;
            width: 100%;
            padding: 0 20px 0 100px;
            text-align: left
        }

        .timeline > li .timeline-panel:before {
            right: auto;
            left: -15px;
            border-right-width: 15px;
            border-left-width: 0
        }

        .timeline > li .timeline-panel:after {
            right: auto;
            left: -14px;
            border-right-width: 14px;
            border-left-width: 0
        }

        .timeline > li .timeline-image {
            position: absolute;
            z-index: 100;
            left: 0;
            width: 80px;
            height: 80px;
            margin-left: 0;
            text-align: center;
            color: #fff;
            border: 7px solid #e9ecef;
            border-radius: 100%;
            background-color: #fed136
        }

        .timeline > li .timeline-image h4 {
            font-size: 10px;
            line-height: 14px;
            margin-top: 12px
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
            padding: 0 20px 0 100px;
            text-align: left
        }

        .timeline > li.timeline-inverted > .timeline-panel:before {
            right: auto;
            left: -15px;
            border-right-width: 15px;
            border-left-width: 0
        }

        .timeline > li.timeline-inverted > .timeline-panel:after {
            right: auto;
            left: -14px;
            border-right-width: 14px;
            border-left-width: 0
        }

        .timeline > li:last-child {
            margin-bottom: 0
        }

        .timeline .timeline-heading h4 {
            margin-top: 0;
            color: inherit
        }

        .timeline .timeline-heading h4.subheading {
            text-transform: none
        }

        .timeline .timeline-body > p, .timeline .timeline-body > ul {
            margin-bottom: 0
        }

        @media (min-width: 768px) {
            .timeline:before {
                left: 50%
            }

            .timeline > li {
                min-height: 100px;
                margin-bottom: 100px
            }

            .timeline > li .timeline-panel {
                float: left;
                width: 41%;
                padding: 0 20px 20px 30px;
                text-align: right
            }

            .timeline > li .timeline-image {
                left: 50%;
                width: 100px;
                height: 100px;
                margin-left: -50px
            }

            .timeline > li .timeline-image h4 {
                font-size: 13px;
                line-height: 18px;
                margin-top: 16px
            }

            .timeline > li.timeline-inverted > .timeline-panel {
                float: right;
                padding: 0 30px 20px 20px;
                text-align: left
            }
        }

        @media (min-width: 992px) {
            .timeline > li {
                min-height: 150px
            }

            .timeline > li .timeline-panel {
                padding: 0 20px 20px
            }

            .timeline > li .timeline-image {
                width: 150px;
                height: 150px;
                margin-left: -75px
            }

            .timeline > li .timeline-image h4 {
                font-size: 18px;
                line-height: 26px;
                margin-top: 30px
            }

            .timeline > li.timeline-inverted > .timeline-panel {
                padding: 0 20px 20px
            }
        }

        @media (min-width: 1200px) {
            .timeline > li {
                min-height: 170px
            }

            .timeline > li .timeline-panel {
                padding: 0 20px 20px 100px
            }

            .timeline > li .timeline-image {
                width: 170px;
                height: 170px;
                margin-left: -85px
            }

            .timeline > li .timeline-image h4 {
                margin-top: 40px
            }

            .timeline > li.timeline-inverted > .timeline-panel {
                padding: 0 100px 20px 20px
            }
        }


    </style>
@endsection

@section('content')
    <div class="container">

        <div class="jumbotron text-center">
            <h1 class="display-4">Hello, world!</h1>
            <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <p class="lead text-center">
                <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
            </p>
        </div>


        {{--<div class="page-header">--}}
        {{--<h1 class="page-title">--}}
        {{--Dashboard--}}
        {{--</h1>--}}
        {{--</div>--}}

        {{----}}

        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">How to start?</h2>
                <hr>
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/1.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2009-2011</h4>
                                <h4 class="subheading">Our Humble Beginnings</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae
                                    sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>March 2011</h4>
                                <h4 class="subheading">An Agency is Born</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae
                                    sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/3.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>December 2012</h4>
                                <h4 class="subheading">Transition to Full Service</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae
                                    sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/4.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>July 2014</h4>
                                <h4 class="subheading">Phase Two Expansion</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae
                                    sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Be Part
                                <br>Of Our
                                <br>Story!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


{{----}}

        <div class="row mb-4">
            <div class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores
                    commodi.</p>
            </div>
            <div class="col-md-4">
                <a class="btn btn-lg btn-secondary btn-block" href="#">Call to Action</a>
            </div>
        </div>

    </div>

@endsection
