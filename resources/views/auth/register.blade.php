@extends('layouts.app')

@section('content')
    <form class="card" method="POST" action="{{ route('register') }}">
        @csrf
        <div class="card-body p-6">
            <div class="card-title">Create new account</div>
            <div class="form-group">
                <label class="form-label">Email address</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label class="form-label">Name</label>
                <input id="email" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label class="form-label">
                    Password
                </label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
            </div>
            <div class="form-group">
                <label class="form-label">
                    Password again
                </label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" required placeholder="Password confirmation">
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" name="tos_read" value="1" required class="custom-control-input">
                    <span class="custom-control-label">Agree with the <a href="{{ route('tos') }}" target="_blank">Terms and Policy</a></span>
                </label>
            </div>
            <div class="form-group">
                We take your privacy data seriously and will only use your personal information to administer your account and to
                provide the services you have requested from us.
                However we would like to send you email of our new products/servic/update we provide. <br><br>If you agree please check Yes below:
            </div>

            <div class="form-group">
                <div class="form-field">
                    <div class="radio"><input type="radio" name="newsletter" value="1">
                        Yes, I would like to be sent by email any communication
                    </div>
                    <span class="newsletter-error error"></span></div>
                <div class="form-field">
                    <div class="radio"><input type="radio" name="newsletter" value="0">
                        No, I&nbsp;do not&nbsp;want to be sent any marketing communications
                    </div>
                    <span class="newsletter-error error"></span></div>
            </div>
            <div class="form-footer">
                <button type="submit" class="btn btn-primary btn-block">Create new account</button>
            </div>
        </div>
    </form>

    <div class="text-center text-muted">
        Already have account? <a href="{{ route('login') }}">Sign in</a>
    </div>

@endsection
