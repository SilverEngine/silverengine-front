<ul class="nav nav-tabs border-0 flex-column flex-lg-row">
    <li class="nav-item">
        <a href="{{ route('home') }}" class="nav-link"><i class="fe fe-home"></i> Home</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('pages') }}" class="nav-link"><i class="fe fe-file-text"></i> Pages</a>
    </li>
</ul>
