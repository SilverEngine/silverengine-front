@extends('layouts.master')

@section('css')
    <style>
        .page-header-layer {
            margin-top: -1.5rem;
            margin-bottom: 1.5rem;
            height: 15rem;
            position: relative;
            margin-bottom: -1.5rem;
            background-color: #333;
            {{--background: url({{ asset("site/01.png") }});--}}
                   background-repeat: no-repeat;
            background-size: cover;
            width: 100%;
        }

        .page-text {
            padding-top: 80px;
            color: white;
        }

        .page-map {
            height: 240px;
            background: #333;
            overflow: hidden;
        }

        .page-map img {
            overflow: hidden;
        }
    </style>
@endsection

@section('content')
    <div class="my-3 my-md-5">
        <div class="map-header">
            <div class="page-header-layer">
                <h2 class="page-text text-center">www.silverengine.net</h2>
            </div>
        </div>
        <div class="container">
            <div class="row row-cards">
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">OpenPenTest</h3>
                        </div>
                        <div class="card-map card-map-placeholder page-map">
                            <a href="#">
                                <img src="{{ asset('site/01.png') }}" alt="">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="media mb-5">
                                <img class="d-flex mr-5 rounded"
                                     src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ec911398e%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ec911398e%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.84375%22%20y%3D%2236.65%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                     alt="Generic placeholder image">
                                <div class="media-body">
                                    Owner information: <br>
                                    @guest
                                        <h5>Login to show more data</h5>
                                    @elseguest
                                        <h5>Luka inc</h5>
                                        <address class="text-muted small">
                                            Use Secure url: yes <br>
                                            <a href="#">https://openpentest.com</a>
                                        </address>
                                    @endguest
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="h6">Views</div>
                                    <p>21313</p>
                                </div>
                                <div class="col-6">
                                    <div class="h6">Business Type</div>
                                    <p>Insurance Company</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{----}}

                    <div class="card">
                        <div class="card-header">
                            <h2 class="card-title">Page status</h2>
                        </div>
                        <table class="table card-table">
                            <tbody>
                            <tr>
                                <td>Statis</td>
                                <td class="text-right">
                                    <span class="badge badge-success">Online</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Page load time</td>
                                <td class="text-right">
                                    <span class="badge badge-default">1.28 seconds</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Sitemap</td>
                                <td class="text-right">
                                    <span class="badge badge-success">YES</span>
                                </td>
                            </tr>
                            <tr>
                                <td>API</td>
                                <td class="text-right">
                                    <span class="badge badge-default">NO</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Platform</td>
                                <td class="text-right">
                                    <span class="badge badge-default">Laravel</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Responsive</td>
                                <td class="text-right">
                                    <span class="badge badge-default">90%</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    {{----}}
                </div>
                <div class="col-lg-8 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Description</h3>
                        </div>
                        <div class="card-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eligendi et harum officia soluta, tempore. Accusamus dicta doloribus earum enim eum facilis illo incidunt
                                ipsa ipsum rem reprehenderit soluta, ullam!</p>
                        </div>
                    </div>


                    {{----}}
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Test penetration level</h2>
                        </div>
                    </div>

                    @for($i = 01; $i < 4; $i++)

                        <div class="card p-3">
                            <div class="d-flex align-items-center">
                                <span class="stamp stamp-md bg-green mr-3">
                                  <i class="fa fa-check"></i>
                                </span>
                                <div>
                                    <h4 class="m-0">Test level {{ $i }} </h4>
                                    <small class="text-muted">Completed!</small>
                                </div>
                            </div>
                        </div>

                    @endfor

                    {{----}}

                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Achievements</h2>
                        </div>
                        @for($i = 0; $i < 9; $i++)
                            @if($i % 2)
                                <div class="col-6 col-sm-4 col-lg-2">
                                    <div class="card">
                                        <div class="card-body p-3 text-center">
                                            <div class="h1 m-0"><i class="fa fa-lock" data-toggle="tooltip" title="" data-original-title="fa fa-binoculars"></i></div>
                                            <div class="text-muted mb-0">Locked</div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-6 col-sm-4 col-lg-2">
                                    <div class="card">
                                        <div class="card-body p-3 text-center">
                                            <div class="h1 m-0"><i class="fa fa-binoculars" data-toggle="tooltip" title="" data-original-title="fa fa-binoculars"></i></div>
                                            <div class="text-muted mb-0">Popular</div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endfor
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Reviews</h2>
                        </div>
                        <div class="col-lg-12">
                            {{----}}
                            @for($i = 0; $i < 5; $i++)
                                <div class="card">
                                    <div class="card-body d-flex flex-column">
                                        <h4>Review: 20.23.2019</h4>
                                        <div class="text-muted">Please do not offer my god a peanut. That's why I love elementary school, Edna. The children beli...</div>
                                        <div class="d-flex align-items-center pt-5 mt-auto">
                                            <div class="avatar avatar-md mr-3" style="background-image: url(./demo/faces/male/4.jpg)"></div>
                                            <div>
                                                <a href="./profile.html" class="text-default">Bobby Knight</a>
                                                <small class="d-block text-muted">3 days ago</small>
                                            </div>
                                            <div class="ml-auto text-muted">
                                                <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                            {{----}}
                        </div>
                    </div>
                    {{----}}
                    @guest
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <h5><a href="{{ route('login') }}">Login for review</a></h5>
                                </div>
                            </div>
                        </div>
                    @else
                        <form class="card">
                            <div class="card-body">
                                <h3 class="card-title">Add review</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <textarea rows="5" class="form-control" placeholder="Here can be your review"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Update Profile</button>
                            </div>
                        </form>
                    @endguest


                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('js')

@endsection
