@extends('layouts.master')

@section('basepath')
    <script>
        requirejs.config({
            baseUrl: '.'
        });
    </script>
@endsection

@section('css')
    <style>
        .pages {
            margin: 40px auto;
        }

        .profiler {
            width: 100%;
            height: auto;
            /*height: 200px;*/
            overflow: hidden;
        }
    </style>
@endsection

@section('content')
    <div class="container pages">
        <div class="row">
            @foreach($pages as $page)
                <div class="col-lg-6 col-xl-4">

                    <div class="card">
                        <a href="{{ route('pages') }}/{{ $page->id }}">
                            <div class="profiler">
                                <img src="{{ $page->image }}" alt="">
                            </div>
                        </a>
                        @if($page->secure)
                            <div class="card-alert alert alert-success mb-0">
                                {{ $page->title }} <span class="pull-right">Secure!</span>
                            </div>
                        @else
                            <div class="card-alert alert alert-danger mb-0">
                                {{ $page->title }} <span class="pull-right">Not safe!</span>
                            </div>
                        @endif
                        <div class="card-body">
                            {{ $page->description }}
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('js')

@endsection
