@extends('layouts.master')

@section('css')
    <style>
        .space {
            margin: 20px auto;
        }

        .timeline {
            position: relative;
            padding: 0;
            list-style: none;
            margin: 20px auto;
        }

        .timeline:before {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 40px;
            width: 2px;
            margin-left: -1.5px;
            content: '';
            background-color: #e9ecef;
        }

        .timeline > li {
            position: relative;
            min-height: 50px;
            margin-bottom: 50px
        }

        .timeline > li:after, .timeline > li:before {
            display: table;
            content: ' '
        }

        .timeline > li:after {
            clear: both
        }

        .timeline > li .timeline-panel {
            position: relative;
            float: right;
            width: 100%;
            padding: 0 20px 0 100px;
            text-align: left
        }

        .timeline > li .timeline-panel:before {
            right: auto;
            left: -15px;
            border-right-width: 15px;
            border-left-width: 0
        }

        .timeline > li .timeline-panel:after {
            right: auto;
            left: -14px;
            border-right-width: 14px;
            border-left-width: 0
        }

        .timeline > li .timeline-image {
            position: absolute;
            z-index: 100;
            left: 0;
            width: 80px;
            height: 80px;
            margin-left: 0;
            text-align: center;
            color: #fff;
            border: 7px solid #e9ecef;
            border-radius: 100%;
            /*background-color: #fed136*/
            background-color: #00c5fe;
        }

        .timeline > li .timeline-image h4 {
            font-size: 10px;
            line-height: 14px;
            margin-top: 12px
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
            padding: 0 20px 0 100px;
            text-align: left
        }

        .timeline > li.timeline-inverted > .timeline-panel:before {
            right: auto;
            left: -15px;
            border-right-width: 15px;
            border-left-width: 0
        }

        .timeline > li.timeline-inverted > .timeline-panel:after {
            right: auto;
            left: -14px;
            border-right-width: 14px;
            border-left-width: 0
        }

        .timeline > li:last-child {
            margin-bottom: 0
        }

        .timeline .timeline-heading h4 {
            /*margin-top: 0;*/
            margin-top: 15px;
            color: inherit
        }

        .timeline .timeline-heading h4.subheading {
            text-transform: none
        }

        .timeline .timeline-body > p, .timeline .timeline-body > ul {
            margin-bottom: 0
        }

        @media (min-width: 768px) {
            .timeline:before {
                left: 50%
            }

            .timeline > li {
                /*min-height: 100px;*/
                margin-bottom: 70px;
                /*margin-bottom: 100px*/
            }

            .timeline > li .timeline-panel {
                float: left;
                width: 41%;
                padding: 0 20px 20px 30px;
                text-align: right
            }

            .timeline > li .timeline-image {
                left: 50%;
                width: 100px;
                height: 100px;
                margin-left: -50px
            }

            .timeline > li .timeline-image h4 {
                font-size: 13px;
                line-height: 18px;
                margin-top: 16px
            }

            .timeline > li.timeline-inverted > .timeline-panel {
                float: right;
                padding: 0 30px 20px 20px;
                text-align: left
            }
        }

        @media (min-width: 992px) {
            .timeline > li {
                /*min-height: 150px*/
            }

            .timeline > li .timeline-panel {
                padding: 0 20px 20px
            }

            .timeline > li .timeline-image {
                width: 150px;
                height: 150px;
                margin-left: -75px
            }

            .timeline > li .timeline-image h4 {
                font-size: 18px;
                line-height: 26px;
                margin-top: 30px
            }

            .timeline > li.timeline-inverted > .timeline-panel {
                padding: 0 20px 20px
            }
        }

        @media (min-width: 1200px) {
            .timeline > li {
                /*min-height: 140px*/
            }

            .timeline > li .timeline-panel {
                padding: 0 20px 20px 100px
            }

            .timeline > li .timeline-image {
                width: 140px;
                height: 140px;
                margin-left: -70px
            }

            .timeline > li .timeline-image h4 {
                margin-top: 26px
            }

            .timeline > li.timeline-inverted > .timeline-panel {
                padding: 0 100px 20px 20px
            }
        }

        .gradientCont {
            background-image: linear-gradient(-270deg, #0085FF 0%, #00FBFF 100%);
            width: 100%;
            height: 300px;
        }

        .mainCont {
            width: 1000px;
            margin: 0 auto;
        }

        .mainCont h2 {
            display: inline-block;
            width: 100%;
            text-align: center;
            /*text-transform: uppercase;*/
            font-weight: 300;
            font-size: 48px;
            margin: 60px 0 0 0;
            color: white;
            opacity: 0;

            animation: fadeInAnimation 1s ease-in-out 0s forwards;
        }

        .mainCont h3 {
            text-align: center;
            font-weight: 400;
            font-size: 18px;
            color: rgba(255, 255, 255, 0.75);
            margin: 0 0 60px 0;
            opacity: 0;

            animation: fadeInAnimation 1s ease-in-out 0.5s forwards;
        }

        .terminalCont {
            background: black;
            width: 100%;
            border-radius: 4px;
            padding: 12px 0 0 0;
            margin-top: -20px;
            font-family: 'Roboto Mono', monospace;
            opacity: 0;

            animation: slideDownAnimation 1s ease-in-out 1s forwards,
            fadeInAnimation 0.8s ease-in-out 1s forwards;
        }

        .userEnteredText {
            color: rgba(255, 255, 255, 0.5);
            margin: 0;
            padding: 0;
            display: inline-block;
        }

        #terminalReslutsCont {
            width: 100%;
            height: 400px;
            padding: 12px;
            overflow-y: auto;
            resize: none;
            border: none;
            font-size: 14px;
            line-height: 28px;
            display: block;
            color: rgba(255, 255, 255, 0.9);
        }

        #terminalReslutsCont a {
            color: rgba(255, 255, 255, 0.9);
            text-decoration: none;
        }

        #terminalReslutsCont a:hover {
            text-decoration: underline;
        }

        #terminalTextInput {
            background: black;
            display: block;
            border: none;
            border-top: 1px solid rgba(255, 255, 255, 0.2);
            border-radius: 0 0 4px 4px;
            width: 100%;
            color: white;
            padding: 18px;
            font-size: 14px;
            outline: none;
            font-family: 'Roboto Mono', monospace;
        }

        /*
  Animation habibi
  Thanks @joericho for the help with this
*/
        @-webkit-keyframes fadeInAnimation {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @-moz-keyframes fadeInAnimation {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @-o-keyframes fadeInAnimation {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @keyframes fadeInAnimation {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @-webkit-keyframes slideDownAnimation {
            0% {
                margin-top: -10px;
            }
            100% {
                margin-top: 0;
            }
        }

        @-moz-keyframes slideDownAnimation {
            0% {
                margin-top: -10px;
            }
            100% {
                margin-top: 0;
            }
        }

        @-o-keyframes slideDownAnimation {
            0% {
                margin-top: -10px;
            }
            100% {
                margin-top: 0;
            }
        }

        @keyframes slideDownAnimation {
            0% {
                margin-top: -10px;
            }
            100% {
                margin-top: 0;
            }
        }

        /* Make me responsive */
        @media screen and (max-width: 1000px) {
            .gradientCont {
                height: 220px;
            }

            .mainCont {
                width: 100%;
                padding: 0 12px;
            }

            .mainCont h2 {
                font-size: 32px;
                margin: 40px 0 0 0;
            }

            .mainCont h3 {
                font-size: 16px;
                margin: 0 0 40px 0;
            }

            #terminalReslutsCont {
                height: 100px;
            }
        }
    </style>
@endsection

@section('content')
    {{--<div class="row">--}}
        <div class="gradientCont">
            <div class="mainCont">
                <h2>SilverEngine Solutions</h2>
                <h3>Framework | CMS | Landing page | API | Protector</h3>
                <div class="terminalCont">
                    <div id="terminalReslutsCont">
                        <div class="app"># ></div>
                    </div>
                    <form>
                        <input id="terminalTextInput" type="text" placeholder="Try typing hi or help..." autocomplete="off">
                    </form>
                </div>
            </div>
        </div>
    {{--</div>--}}
    <div class="container">

        <div class="row" style="margin-top: 450px;">
            <div class="col-lg-12">
                <h2 class="text-center">How to start?</h2>
                <hr>
                <ul class="timeline">
                    {{----}}
                    <li>
                        <div class="timeline-image">
                            <h4 style="margin-top: 50px;">Step 1</h4>
                            {{--<img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">--}}
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading">Register</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">To evaluate your website you first need to register on our application and verified your registration. After email validation select the right
                                    tool that you want to use it.</p>
                            </div>
                        </div>
                    </li>
                    {{----}}
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4 style="margin-top: 50px;">Step 2</h4>
                            {{--<img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">--}}
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading">Generate secret token</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">After registration click on Submit new website. After submiting you will get special file to upload on your server. This code will give us
                                    validation that your website is yours and you can publish for the review!</p>
                            </div>
                        </div>
                    </li>
                    {{----}}
                    <li>
                        <div class="timeline-image">
                            <h4 style="margin-top: 50px;">Step 3</h4>
                            {{--<img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">--}}
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading">Submit your page</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">When you submiting the pageto our platform our moderators will check your page as first layer of security. after approoving our members will give
                                    a review. </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4 style="margin-top: 50px;">Step 4</h4>
                            {{--<img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">--}}
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading">Many level of security</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">After getting first level of security our engine will give several tests that will determinated how secure your website is!</p>
                            </div>
                        </div>
                    </li>

                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4 style="margin-top: 50px;">Success!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        {{----}}

        <div class="space" style="margin-top: 150px; margin-bottom: 50px;">
            <div class="row mb-4">
                <div class="col-md-8">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores
                        commodi.</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-secondary btn-block" href="#">Call to Action</a>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://www.mattboldt.com/demos/typed-js/js/typed.custom.js"></script>

    <script>
        $(function(){
            $(".app").typed({
                strings: [" ", " ",  " ", " ",  " Welcome to SilverEngine Solutions", "Protect secure and optimize your website"],
                cursorChar: "█",
                typeSpeed: 0
            });
        });
    </script>
@endsection
