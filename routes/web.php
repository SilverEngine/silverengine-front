<?php


Route::get('/', 'v1\HomeController@index')->name('home');
Route::get('/home', 'v1\HomeController@index')->name('home');
Route::get('/docs', 'v1\HomeController@index')->name('docs');
Route::get('/pages/{id?}', 'v1\HomeController@pages')->name('pages');
Route::post('/search', 'v1\HomeController@search')->name('pages_search');

Auth::routes();

Route::get('/tos', 'v1\HomeController@tos')->name('tos');
Route::get('/privacy-policy', 'v1\HomeController@privacy')->name('policy');
